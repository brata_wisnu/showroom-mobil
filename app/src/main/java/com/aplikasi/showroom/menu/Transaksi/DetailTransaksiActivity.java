package com.aplikasi.showroom.menu.Transaksi;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplikasi.showroom.R;
import com.aplikasi.showroom.Utils.Utilities;

import java.text.NumberFormat;
import java.util.Locale;

public class DetailTransaksiActivity extends AppCompatActivity {
    Activity activity;
    Button btnEdit, btnKembali;
    Toolbar toolbar;
    EditText etIdPesanan, etID, etName, etTelepon, etEmail, etAlamat, etTanggal, etMerk, etHarga, etWarna, etType, etPembayaran, etNote;
    ImageView imageView;
    TextView tvMerk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi);
        activity = this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        btnKembali = findViewById(R.id.btnBack);
        btnEdit = findViewById(R.id.btnEdit);

        imageView = findViewById(R.id.imageView);
        tvMerk = findViewById(R.id.tvMerk);

        etIdPesanan = findViewById(R.id.etIdPesanan);
        etID = findViewById(R.id.etIdMobil);
        etName = findViewById(R.id.etNama);
        etTelepon = findViewById(R.id.etTelepon);
        etEmail = findViewById(R.id.etEmail);
        etAlamat = findViewById(R.id.etAlamat);
        etTanggal = findViewById(R.id.etTanggal);
        etMerk = findViewById(R.id.etMerk);
        etHarga = findViewById(R.id.etHarga);
        etWarna = findViewById(R.id.etWarna);
        etType = findViewById(R.id.etType);
        etPembayaran = findViewById(R.id.etPembayaran);
        etNote = findViewById(R.id.etNote);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final String strIdPesanan = getIntent().getStringExtra("id_pesanan") != null ? getIntent().getStringExtra("id_pesanan") : "";
        final String strID = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
        final String strNama = getIntent().getStringExtra("nama") != null ? getIntent().getStringExtra("nama") : "";
        final String strTelepon = getIntent().getStringExtra("telepon") != null ? getIntent().getStringExtra("telepon") : "";
        final String strEmail = getIntent().getStringExtra("email") != null ? getIntent().getStringExtra("email") : "";
        final String strAlamat = getIntent().getStringExtra("alamat") != null ? getIntent().getStringExtra("alamat") : "";
        final String strTanggal = getIntent().getStringExtra("tanggal") != null ? getIntent().getStringExtra("tanggal") : "";
        final String strMerk = getIntent().getStringExtra("merk") != null ? getIntent().getStringExtra("merk") : "";
        final String strHarga = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
        final String strWarna = getIntent().getStringExtra("warna") != null ? getIntent().getStringExtra("warna") : "";
        final String strType = getIntent().getStringExtra("type") != null ? getIntent().getStringExtra("type") : "";
        final String strPembayaran = getIntent().getStringExtra("pembayaran") != null ? getIntent().getStringExtra("pembayaran") : "";
        final String strImage = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";

        //Format Rupiah
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
//        final int idDrawable = activity.getResources().getIdentifier(strImage, "drawable", activity.getPackageName());

        // TODO: Cek Gambar
        if (strImage.equals("images_civic")){
            imageView.setImageResource(R.drawable.images_civic);
        }else if (strImage.equals("images_jazz_silver")){
            imageView.setImageResource(R.drawable.images_jazz_silver);
        }else if (strImage.equals("images_terios")){
            imageView.setImageResource(R.drawable.images_terios);
        }else if (strImage.equals("images_mclaren")){
            imageView.setImageResource(R.drawable.images_mclaren);
        }else if (strImage.equals("images_ford")){
            imageView.setImageResource(R.drawable.images_ford);
        }else if (strImage.equals("images_jazz")){
            imageView.setImageResource(R.drawable.images_jazz);
        }else if (strImage.equals("images_avanza")){
            imageView.setImageResource(R.drawable.images_avanza);
        }else if (strImage.equals("images_hrv")){
            imageView.setImageResource(R.drawable.images_hrv);
        }else if (strImage.equals("images_hrv_red")){
            imageView.setImageResource(R.drawable.images_hrv_red);
        }
        tvMerk.setText(strMerk);

        etIdPesanan.setText(strIdPesanan);
        etID.setText(strID);
        etName.setText(strNama);
        etTelepon.setText(strTelepon);
        etEmail.setText(strEmail);
        etAlamat.setText(strAlamat);
        etTanggal.setText(strTanggal);
        etMerk.setText(strMerk);
        etHarga.setText(formatRupiah.format((double) Integer.parseInt(strHarga)) + ",-");
        etWarna.setText(strWarna);
        etType.setText(strType);
        etPembayaran.setText(strPembayaran);

        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Anda yakin akan mengubah data?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                Intent i = new Intent(activity, EditTransaksiActivity.class);
                                i.putExtra("id_pesanan", strIdPesanan);
                                i.putExtra("id", strID);
                                i.putExtra("nama", strNama);
                                i.putExtra("telepon", strTelepon);
                                i.putExtra("email", strEmail);
                                i.putExtra("alamat", strAlamat);
                                i.putExtra("tanggal", strTanggal);
                                i.putExtra("merk", strMerk);
                                i.putExtra("harga", strHarga);
                                i.putExtra("warna", strWarna);
                                i.putExtra("type", strType);
                                i.putExtra("pembayaran", strPembayaran);
                                i.putExtra("image", strImage);
                                startActivity(i);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
