package com.aplikasi.showroom.menu.Home.item_slider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplikasi.showroom.menu.DetailMobil;
import com.aplikasi.showroom.R;

public class LayoutOne extends Fragment {

    Button btnDetail;
    ImageView imgView;
    TextView tvMerk;
    TextView tvDescription;

    public static Fragment newInstance(Context context) {
        LayoutOne f = new LayoutOne();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup vi = (ViewGroup) inflater.inflate(R.layout.layout_slider_item, null);
        btnDetail       = vi.findViewById(R.id.btnDetail);
        imgView         = vi.findViewById(R.id.imageView);
        tvMerk          = vi.findViewById(R.id.tvMerk);
        tvDescription   = vi.findViewById(R.id.textView1);

        final String strID = "M0455";
        final String strMobil = "Ford Fiesta 1.5 EcoBoost ST-3 3dr";
        final String strImage = "images_ford";
        final String strHarga = "510000000";
        final String strDescription = getResources().getString(R.string.desc_ford);

        //final Context context = imgView.getContext();
        //int id = context.getResources().getIdentifier(strImage, "drawable", context.getPackageName());
        imgView.setImageResource(R.drawable.images_ford);
        tvDescription.setText(strDescription);
        tvMerk.setText(strMobil);
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), DetailMobil.class);
                i.putExtra("id", strID);
                i.putExtra("mobil", strMobil);
                i.putExtra("image", strImage);
                i.putExtra("harga", strHarga);
                i.putExtra("description", strDescription);
                startActivity(i);


            }
        });
        return vi;
    }

}
