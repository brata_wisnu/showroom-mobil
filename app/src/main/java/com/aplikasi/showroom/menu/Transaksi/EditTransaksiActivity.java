package com.aplikasi.showroom.menu.Transaksi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.aplikasi.showroom.menu.KonfirmasiPesananActivity;
import com.aplikasi.showroom.R;
import com.aplikasi.showroom.Utils.Utilities;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class EditTransaksiActivity extends AppCompatActivity {
    Activity activity;
    Button btnSubmit;
    Toolbar toolbar;
    TextInputLayout txtIdPesanan;
    EditText etIdPesanan, etID, etName, etTelepon, etEmail, etAlamat, etTanggal, etMerk,etHarga, etWarna;
    Spinner spWarna,spType,spPembayaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_customer);
        activity = this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        btnSubmit   = findViewById(R.id.btnSubmit);
        txtIdPesanan= findViewById(R.id.txtIdPesanan);
        etIdPesanan = findViewById(R.id.etIdPesanan);
        etID        = findViewById(R.id.etIdMobil);
        etName      = findViewById(R.id.etNama);
        etTelepon   = findViewById(R.id.etTelepon);
        etEmail     = findViewById(R.id.etEmail);
        etAlamat    = findViewById(R.id.etAlamat);
        etTanggal   = findViewById(R.id.etTanggal);
        etMerk      = findViewById(R.id.etMerk);
        etHarga     = findViewById(R.id.etHarga);
        etWarna     = findViewById(R.id.etWarna);
        spWarna     = findViewById(R.id.sp_warna);
        spType      = findViewById(R.id.sp_type);
        spPembayaran= findViewById(R.id.sp_pembayaran);

        toolbar     = (Toolbar) findViewById(R.id.toolbar);
        TextView title = findViewById(R.id.titleHeader);
        title.setText("Edit Transaksi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        String strIdPesanan = getIntent().getStringExtra("id_pesanan") != null ? getIntent().getStringExtra("id_pesanan") : "";
        String strID        = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
        String strNama      = getIntent().getStringExtra("nama") != null ? getIntent().getStringExtra("nama") : "";
        String strTelepon   = getIntent().getStringExtra("telepon") != null ? getIntent().getStringExtra("telepon") : "";
        String strEmail     = getIntent().getStringExtra("email") != null ? getIntent().getStringExtra("email") : "";
        String strAlamat    = getIntent().getStringExtra("alamat") != null ? getIntent().getStringExtra("alamat") : "";
        String strTanggal   = getIntent().getStringExtra("tanggal") != null ? getIntent().getStringExtra("tanggal") : "";
        String strMerk      = getIntent().getStringExtra("merk") != null ? getIntent().getStringExtra("merk") : "";
        String strHarga     = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
        String strWarna     = getIntent().getStringExtra("warna") != null ? getIntent().getStringExtra("warna") : "";
        String strType      = getIntent().getStringExtra("type") != null ? getIntent().getStringExtra("type") : "";
        String strPembayaran= getIntent().getStringExtra("pembayaran") != null ? getIntent().getStringExtra("pembayaran") : "";
        String strImage     = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";


        String[] WarnaMobil = {"- Pilih Warna Mobil -","Merah", "Putih", "Hitam", "Biru"};
        String[] TypeMobil = {"- Pilih Type Mobil -","Manual", "Matic"};
        String[] MetodePembayaran = {"- Pilih Metode Pembayaran -","Cash", "Kredit"};

        SpinnerView(spWarna, WarnaMobil);
        SpinnerView(spType, TypeMobil);
        SpinnerView(spPembayaran, MetodePembayaran);
        String strTgl = Utilities.ConvertDateFormat(Utilities.currentDateFormat());

        //Format Rupiah
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        txtIdPesanan.setVisibility(View.VISIBLE);
        etIdPesanan.setText(strIdPesanan);
        etID.setText(strID);
        etName.setText(strNama);
        etTelepon.setText(strTelepon);
        etEmail.setText(strEmail);
        etAlamat.setText(strAlamat);
        etTanggal.setText(strTanggal);
        etMerk.setText(strMerk);
        etHarga.setText(formatRupiah.format((double) Integer.parseInt(strHarga))+",-");
        //etWarna.setText(strWarna);

        //selection Warna Mobil
        if (strWarna.equals("Merah")){
            spWarna.setSelection(1);
        }else if (strWarna.equals("Putih")){
            spWarna.setSelection(2);
        }else if (strWarna.equals("Hitam")){
            spWarna.setSelection(3);
        }else if (strWarna.equals("Biru")){
            spWarna.setSelection(4);
        }else
            spWarna.setSelection(0);

        //selection Type Mobil
        if (strType.equals("Manual")){
            spType.setSelection(1);
        }else if (strType.equals("Matic")){
            spType.setSelection(2);
        }else
            spType.setSelection(0);

        //selection Metode Pembayaran
        if (strPembayaran.equals("Cash")){
            spPembayaran.setSelection(1);
        }else if (strPembayaran.equals("Kredit")){
            spPembayaran.setSelection(2);
        }else
            spPembayaran.setSelection(0);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickSubmit();
            }
        });
    }

    private void SpinnerView(Spinner sp, String[] listItem) {
        final List<String> listMeja = new ArrayList<>(Arrays.asList(listItem));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,listMeja);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp.setAdapter(spinnerArrayAdapter);
        // mengeset listener untuk mengetahui saat item dipilih
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                //Toast.makeText(activity, spinnerArrayAdapter.getItem(i), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void ClickSubmit() {
        String strIdPesanan = etIdPesanan.getText().toString().trim();
        String strID        = etID.getText().toString().trim();
        String strNama      = etName.getText().toString().trim();
        String strTelepon   = etTelepon.getText().toString();
        String strEmail     = etEmail.getText().toString();
        String strAlamat    = etAlamat.getText().toString();
        String strTanggal   = etTanggal.getText().toString();
        String strMerk      = etMerk.getText().toString();
        //String strWarna     = etWarna.getText().toString();
        String strWarna      = spWarna.getSelectedItem().toString();
        String strType      = spType.getSelectedItem().toString();
        String strPembayaran= spPembayaran.getSelectedItem().toString();

        if (!Utilities.isValidForm(activity,strID,etID)){
            return;
        }
        if (!Utilities.isValidForm(activity,strNama,etName)){
            return;
        }
        if (!Utilities.isValidForm(activity,strTelepon,etTelepon)){
            return;
        }
        if (!Utilities.isValidForm(activity,strEmail,etEmail)){
            return;
        }
        if (!Utilities.isValidForm(activity,strAlamat,etAlamat)){
            return;
        }
        if (!Utilities.isValidForm(activity,strTanggal,etTanggal)){
            return;
        }
        if (!Utilities.isValidForm(activity,strMerk,etMerk)){
            return;
        }
        if (strType.equals("- Pilih Warna Mobil -")){
            Utilities.ShowToast(activity, "pilih warna mobil");
            return;
        }
        if (strType.equals("- Pilih Type Mobil -")){
            Utilities.ShowToast(activity, "pilih type mobil");
            return;
        }
        if (strPembayaran.equals("- Pilih Metode Pembayaran -")){
            Utilities.ShowToast(activity, "pilih metode pembayaran");
            return;
        }


        String strImage     = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";
        String strHarga     = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";


        Intent i = new Intent(activity, KonfirmasiPesananActivity.class);
        i.putExtra("formType", "edit_form");
        i.putExtra("id_pesanan", strIdPesanan);
        i.putExtra("id", strID);
        i.putExtra("nama", strNama);
        i.putExtra("telepon", strTelepon);
        i.putExtra("email", strEmail);
        i.putExtra("alamat", strAlamat);
        i.putExtra("tanggal", strTanggal);
        i.putExtra("merk", strMerk);
        i.putExtra("harga", strHarga);
        i.putExtra("warna", strWarna);
        i.putExtra("type", strType);
        i.putExtra("pembayaran", strPembayaran);
        i.putExtra("image", strImage);
        startActivity(i);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
