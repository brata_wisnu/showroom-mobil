package com.aplikasi.showroom.menu.Mobil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.aplikasi.showroom.Utils.Utilities;
import com.aplikasi.showroom.menu.DetailMobil;
import com.aplikasi.showroom.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mobil extends Fragment {

    TextView empty;
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list_mobil, container, false);
        empty       = v.findViewById(R.id.empty);
        listView    = v.findViewById(R.id.listview);

        listView.setAdapter(null);
        listView.setVisibility(View.VISIBLE);

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> data1 = new HashMap<>();

        data1.put("id_", "M0451");
        data1.put("image", "images_civic");
        data1.put("nama_mobil", "Honda Civic EX");
        data1.put("harga", "182000000");
        data1.put("description", getResources().getString(R.string.desc_civic));
        list.add(data1);


        HashMap<String, String> data2 = new HashMap<>();
        data2.put("id_", "M0452");
        data2.put("image", "images_jazz_silver");
        data2.put("nama_mobil", "Honda Jazz 2019 V i-VTEC 1.5");
        data2.put("harga", "320000000");
        data2.put("description", getResources().getString(R.string.desc_jazz));
        list.add(data2);


        HashMap<String, String> data3 = new HashMap<>();
        data3.put("id_", "M0453");
        data3.put("image", "images_terios");
        data3.put("nama_mobil", "Daihatsu Terios 2017 CUSTOM 1.5");
        data3.put("harga", "240000000");
        data3.put("description", getResources().getString(R.string.desc_terios));
        list.add(data3);

        HashMap<String, String> data4 = new HashMap<>();
        data4.put("id_", "M0454");
        data4.put("image", "images_hrv_red");
        data4.put("nama_mobil", "Honda HR-V 2018 E 1.5");
        data4.put("harga", "450000000");
        data4.put("description", getResources().getString(R.string.desc_hrv));
        list.add(data4);

        HashMap<String, String> data5 = new HashMap<>();
        data5.put("id_", "M0455");
        data5.put("image", "images_ford");
        data5.put("nama_mobil", "Ford Fiesta 1.5 EcoBoost ST-3 3dr");
        data5.put("harga", "510000000");
        data5.put("description", getResources().getString(R.string.desc_ford));
        list.add(data5);

        HashMap<String, String> data6 = new HashMap<>();
        data6.put("id_", "M0456");
        data6.put("image", "images_hrv");
        data6.put("nama_mobil", "Honda HR-V 2018 E 1.5");
        data6.put("harga", "450000000");
        data6.put("description", getResources().getString(R.string.desc_hrv));
        list.add(data6);

        ListAdapter adapter = new ListAdapter(getContext(), list,
                R.layout.item_mobil, new String[]{"id_", "nama_mobil", "harga", "description"},
                new int[]{R.id.imageView,R.id.btnDetail,});
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();

        return v;
    }


    // TODO: List Adapter Barcode Scanning
    public class ListAdapter extends SimpleAdapter {
        private Context mContext;
        public LayoutInflater inflater = null;

        public ListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.item_mobil, null);

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            final LinearLayout _id = vi.findViewById(R.id.item);
            final ImageView _image = vi.findViewById(R.id.imageView);
            final TextView _merk = vi.findViewById(R.id.tvMerk);
            final TextView _btnDetail = vi.findViewById(R.id.btnDetail);

            final String strID = (String) data.get("id_");
            final String strImage = (String) data.get("image");
            final String strMobil = (String) data.get("nama_mobil");
            final String strHarga = (String) data.get("harga");
            final String strDescription = (String) data.get("description");

            _btnDetail.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    Intent i = new Intent(mContext, DetailMobil.class);
                    i.putExtra("id", strID);
                    i.putExtra("mobil", strMobil);
                    i.putExtra("image", strImage);
                    i.putExtra("harga", strHarga);
                    i.putExtra("description", strDescription);
                    startActivity(i);
                    return true;
                }
            });

            _btnDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, DetailMobil.class);
                    i.putExtra("id", strID);
                    i.putExtra("image", strImage);
                    i.putExtra("mobil", strMobil);
                    i.putExtra("harga", strHarga);
                    i.putExtra("description", strDescription);
                    startActivity(i);
                }
            });

            Utilities.ShowLog("images",strImage);
//            Context context = _image.getContext();
//            int id = context.getResources().getIdentifier(strImage, "drawable", context.getPackageName());

            // TODO: Cek Gambar
            if (strImage.equals("images_civic")){
                _image.setImageResource(R.drawable.images_civic);
            }else if (strImage.equals("images_jazz_silver")){
                _image.setImageResource(R.drawable.images_jazz_silver);
            }else if (strImage.equals("images_terios")){
                _image.setImageResource(R.drawable.images_terios);
            }else if (strImage.equals("images_mclaren")){
                _image.setImageResource(R.drawable.images_mclaren);
            }else if (strImage.equals("images_ford")){
                _image.setImageResource(R.drawable.images_ford);
            }else if (strImage.equals("images_jazz")){
                _image.setImageResource(R.drawable.images_jazz);
            }else if (strImage.equals("images_avanza")){
                _image.setImageResource(R.drawable.images_avanza);
            }else if (strImage.equals("images_hrv")){
                _image.setImageResource(R.drawable.images_hrv);
            }else if (strImage.equals("images_hrv_red")){
                _image.setImageResource(R.drawable.images_hrv_red);
            }
            _merk.setText(strMobil);
            return vi;
        }
    }

}
