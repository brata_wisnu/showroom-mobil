package com.aplikasi.showroom.menu.Home.item_slider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplikasi.showroom.menu.DetailMobil;
import com.aplikasi.showroom.R;

public class LayoutTwo extends Fragment {

    Button btnDetail;
    ImageView imgView;
    TextView tvMerk;
    TextView tvDescription;

    public static Fragment newInstance(Context context) {
        LayoutTwo f = new LayoutTwo();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup vi = (ViewGroup) inflater.inflate(R.layout.layout_slider_item, null);
        btnDetail       = vi.findViewById(R.id.btnDetail);
        imgView         = vi.findViewById(R.id.imageView);
        tvMerk          = vi.findViewById(R.id.tvMerk);
        tvDescription   = vi.findViewById(R.id.textView1);

        final String strID = "M0458";
        final String strMobil = "Honda Jazz 2019 V i-VTEC 1.5";
        final String strImage = "images_jazz";
        final String strHarga = "320000000";
        final String strDescription = getResources().getString(R.string.desc_jazz);

//        final Context context = imgView.getContext();
//        int id = context.getResources().getIdentifier(strImage, "drawable", context.getPackageName());

        imgView.setImageResource(R.drawable.images_jazz);
        tvMerk.setText(strMobil);
        tvDescription.setText(strDescription);

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), DetailMobil.class);
                i.putExtra("id", strID);
                i.putExtra("mobil", strMobil);
                i.putExtra("image", strImage);
                i.putExtra("harga", strHarga);
                i.putExtra("description", strDescription);
                startActivity(i);
            }
        });
        return vi;

    }
}
