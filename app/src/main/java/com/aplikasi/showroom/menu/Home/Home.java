package com.aplikasi.showroom.menu.Home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.aplikasi.showroom.R;
import com.aplikasi.showroom.menu.Home.item_slider.LayoutOne;
import com.aplikasi.showroom.menu.Home.item_slider.LayoutThree;
import com.aplikasi.showroom.menu.Home.item_slider.LayoutTwo;

import me.relex.circleindicator.CircleIndicator;

public class Home extends Fragment {
    private ViewPager _mViewPager;
    private ViewPagerAdapter _adapter;

    public Home() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.fragment_home, container, false);
        setUpView(vi);
        setTab();
        return vi;
    }

    private void setUpView(View v) {
        _mViewPager = (ViewPager) v.findViewById(R.id.viewPager);
        _adapter    = new ViewPagerAdapter(getContext(), getChildFragmentManager());
        _mViewPager.setAdapter(_adapter);
        _mViewPager.setCurrentItem(0);

        CircleIndicator indicator = (CircleIndicator) v.findViewById(R.id.indicator);
        indicator.setViewPager(_mViewPager);

        _adapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    private void setTab() {
        _mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub


            }

        });

    }

}

class ViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage=3;
    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context=context;
    }
    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();

        switch(position){
            case 0:
                f= LayoutOne.newInstance(_context);
                break;
            case 1:
                f= LayoutTwo.newInstance(_context);
                break;
            case 2:
                f= LayoutThree.newInstance(_context);
                break;
        }
        return f;
    }
    @Override
    public int getCount() {
        return totalPage;
    }

}

