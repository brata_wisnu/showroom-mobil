package com.aplikasi.showroom.menu.Transaksi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.aplikasi.showroom.LoginActivity;
import com.aplikasi.showroom.MainActivity;
import com.aplikasi.showroom.R;
import com.aplikasi.showroom.Utils.SharedPrefManager;
import com.aplikasi.showroom.Utils.Utilities;
import com.aplikasi.showroom.Utils.DatabaseLabel;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Transaksi extends Fragment {

    TextView empty;
    ListView listView;
    Button btnLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list_transaksi, container, false);
        empty       = v.findViewById(R.id.empty);
        listView    = v.findViewById(R.id.listview);
        btnLogin    = v.findViewById(R.id.btnLogin);

        listView.setAdapter(null);
        listView.setVisibility(View.VISIBLE);

        return v;
    }

    public void getListData() {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        SQLiteDatabase database = getActivity().openOrCreateDatabase(DatabaseLabel.DB_NAME, Context.MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseLabel.TABLE_TRANSAKSI + "(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_PESANAN TEXT, DATA_JSON TEXT);");
        Cursor cursor = database.rawQuery("select * from "+ DatabaseLabel.TABLE_TRANSAKSI +" order by number DESC" , null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> transaksi = new HashMap<>();
                String strIdPesanan = cursor.getString(cursor.getColumnIndex("ID_PESANAN"));
                String strData = cursor.getString(cursor.getColumnIndex("DATA_JSON"));

                try {
                    JSONObject obj = new JSONObject(strData);
                    String strMerk = obj.optString("merk");
                    String strNama = obj.optString("nama");
                    String strTanggal = obj.optString("tanggal");

                    transaksi.put("id_pesanan", strIdPesanan);
                    transaksi.put("merk", strMerk);
                    transaksi.put("customer", strNama);
                    transaksi.put("waktu", strTanggal);
                    transaksi.put("data_json", strData);
                    list.add(transaksi);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } while (cursor.moveToNext());
            empty.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        cursor.close();
        database.close();
        ListAdapter adapter = new ListAdapter(getContext(), list,
                R.layout.item_transaksi, new String[]{"merk", "customer", "meja", "waktu", "data_json"},
                new int[]{R.id.linID, R.id.tvMerk, R.id.tvCustomer, R.id.tvDate});
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null)
        if (SharedPrefManager.getInstance(getContext()).isLoggedIn()){
            btnLogin.setVisibility(View.GONE);
            getListData();
        }else {
            btnLogin.setVisibility(View.VISIBLE);
            empty.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getContext(), LoginActivity.class);
                    i.putExtra("intent", "transaksi");
                    startActivity(i);
                }
            });
        }
    }

    // TODO: List Adapter Barcode Scanning
    public class ListAdapter extends SimpleAdapter {
        private Context mContext;
        public LayoutInflater inflater = null;

        public ListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.item_transaksi, null);

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            final LinearLayout _item    = vi.findViewById(R.id.linID);
            final TextView _merk        = vi.findViewById(R.id.tvMerk);
            final TextView _customer    = vi.findViewById(R.id.tvCustomer);
            final TextView _date        = vi.findViewById(R.id.tvDate);

            final String strIdPesanan = (String) data.get("id_pesanan");
            final String strMerk = (String) data.get("merk");
            final String strCustomer = (String) data.get("customer");
            final String strWaktu = (String) data.get("waktu");
            final String strJson = (String) data.get("data_json");

            _item.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Anda yakin ingin menghapus?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    SQLiteDatabase database = getActivity().openOrCreateDatabase(DatabaseLabel.DB_NAME, Context.MODE_PRIVATE, null);
                                    database.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseLabel.TABLE_TRANSAKSI + "(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_PESANAN TEXT, DATA_JSON TEXT);");
                                    database.execSQL("DELETE FROM "+ DatabaseLabel.TABLE_TRANSAKSI +" where ID_PESANAN = '" + strIdPesanan + "';");
                                    database.close();
                                    getListData();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                    return true;
                }
            });

            _item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClickItemTransaksi(strJson);
                }
            });

            _merk.setText(strMerk);
            _customer.setText(strCustomer);
            _date.setText(strWaktu);
            return vi;
        }

        private void ClickItemTransaksi(String strJson) {
            try {
                JSONObject obj = new JSONObject(strJson);

                String strIdPesanan = obj.optString("id_pesanan");
                String strID        = obj.optString("id");
                String strCustomer  = obj.optString("nama");
                String strTelepon   = obj.optString("telepon");
                String strEmail     = obj.optString("email");
                String strAlamat    = obj.optString("alamat");
                String strTanggal   = obj.optString("tanggal");
                String strMerk      = obj.optString("merk");
                String strHarga     = obj.optString("harga");
                String strWarna     = obj.optString("warna");
                String strType      = obj.optString("type");
                String strPembayaran= obj.optString("pembayaran");
                String strImage     = obj.optString("image");


                Intent i = new Intent(mContext, DetailTransaksiActivity.class);
                i.putExtra("id_pesanan",strIdPesanan);
                i.putExtra("id",strID);
                i.putExtra("nama",strCustomer);
                i.putExtra("telepon",strTelepon);
                i.putExtra("email",strEmail);
                i.putExtra("alamat",strAlamat);
                i.putExtra("tanggal",strTanggal);
                i.putExtra("merk",strMerk);
                i.putExtra("harga",strHarga);
                i.putExtra("warna",strWarna);
                i.putExtra("type",strType);
                i.putExtra("pembayaran",strPembayaran);
                i.putExtra("image", strImage);
                startActivity(i);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
