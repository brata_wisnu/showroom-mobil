package com.aplikasi.showroom.menu;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplikasi.showroom.LoginActivity;
import com.aplikasi.showroom.MainActivity;
import com.aplikasi.showroom.R;
import com.aplikasi.showroom.Utils.SharedPrefManager;

import java.text.NumberFormat;
import java.util.Locale;

public class DetailMobil extends AppCompatActivity {
    Activity activity;
    ImageView imgView;
    TextView tvDescription;
    TextView tvHarga;
    TextView tvMerk;
    Button btnPesan;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mobil);
        activity = this;

        imgView         = findViewById(R.id.imageView);
        tvDescription   = findViewById(R.id.description);
        tvHarga         = findViewById(R.id.textHarga);
        tvMerk          = findViewById(R.id.tvMerk);
        btnPesan        = findViewById(R.id.btnPesan);
        toolbar         = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getDetailMobil();
    }

    private void getDetailMobil() {
        final String id     = getIntent().getStringExtra("id")!=null?getIntent().getStringExtra("id"):"";
        final String image  = getIntent().getStringExtra("image")!=null?getIntent().getStringExtra("image"):"";
        final String mobil  = getIntent().getStringExtra("mobil")!=null?getIntent().getStringExtra("mobil"):"";
        final String harga  = getIntent().getStringExtra("harga")!=null?getIntent().getStringExtra("harga"):"0";
        final String description = getIntent().getStringExtra("description")!=null?getIntent().getStringExtra("description"):"";

        //final int idDrawable = activity.getResources().getIdentifier(image, "drawable", activity.getPackageName());

        // TODO: Cek Gambar
        if (image.equals("images_civic")){
            imgView.setImageResource(R.drawable.images_civic);
        }else if (image.equals("images_jazz_silver")){
            imgView.setImageResource(R.drawable.images_jazz_silver);
        }else if (image.equals("images_terios")){
            imgView.setImageResource(R.drawable.images_terios);
        }else if (image.equals("images_mclaren")){
            imgView.setImageResource(R.drawable.images_mclaren);
        }else if (image.equals("images_ford")){
            imgView.setImageResource(R.drawable.images_ford);
        }else if (image.equals("images_jazz")){
            imgView.setImageResource(R.drawable.images_jazz);
        }else if (image.equals("images_avanza")){
            imgView.setImageResource(R.drawable.images_avanza);
        }else if (image.equals("images_hrv")){
            imgView.setImageResource(R.drawable.images_hrv);
        }else if (image.equals("images_hrv_red")){
            imgView.setImageResource(R.drawable.images_hrv_red);
        }

        //Format Rupiah
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        tvMerk.setText(mobil);
        tvDescription.setText(description);
        tvHarga.setText(formatRupiah.format((double) Integer.parseInt(harga))+",-");


        btnPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SharedPrefManager.getInstance(activity).isLoggedIn()){
                    Intent i = new Intent(activity, FormCustomerActivity.class);
                    i.putExtra("id", id);
                    i.putExtra("mobil", mobil);
                    i.putExtra("image", image);
                    i.putExtra("harga", harga);
                    i.putExtra("description", description);
                    startActivity(i);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Anda harus login terlebih dahulu?")
                            .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(activity, LoginActivity.class);
                                    i.putExtra("intent", "detailMobil");
                                    i.putExtra("id", id);
                                    i.putExtra("mobil", mobil);
                                    i.putExtra("image", image);
                                    i.putExtra("harga", harga);
                                    i.putExtra("description", description);
                                    startActivity(i);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDetailMobil();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(activity, MainActivity.class);
        startActivity(i);
        finish();

    }
}
