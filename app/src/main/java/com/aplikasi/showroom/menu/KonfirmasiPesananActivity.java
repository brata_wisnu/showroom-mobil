package com.aplikasi.showroom.menu;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplikasi.showroom.MainActivity;
import com.aplikasi.showroom.R;
import com.aplikasi.showroom.Utils.DatabaseLabel;
import com.aplikasi.showroom.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class KonfirmasiPesananActivity extends AppCompatActivity {
    Activity activity;
    Button btnSubmit;
    Toolbar toolbar;
    EditText etIDPesanan, etID, etName, etTelepon, etEmail, etAlamat, etTanggal, etMerk,etHarga, etWarna, etType, etPembayaran, etNote;
    ImageView imageView;
    TextView tvMerk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan);
        activity = this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        btnSubmit   = findViewById(R.id.btnSubmit);
        imageView   = findViewById(R.id.imageView);
        tvMerk      = findViewById(R.id.tvMerk);
        etIDPesanan = findViewById(R.id.etIdPesanan);
        etID        = findViewById(R.id.etIdMobil);
        etName      = findViewById(R.id.etNama);
        etTelepon   = findViewById(R.id.etTelepon);
        etEmail     = findViewById(R.id.etEmail);
        etAlamat    = findViewById(R.id.etAlamat);
        etTanggal   = findViewById(R.id.etTanggal);
        etMerk      = findViewById(R.id.etMerk);
        etHarga     = findViewById(R.id.etHarga);
        etWarna     = findViewById(R.id.etWarna);
        etType      = findViewById(R.id.etType);
        etPembayaran= findViewById(R.id.etPembayaran);
        etNote      = findViewById(R.id.etNote);

        toolbar     = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        String strIdPesanan = getIntent().getStringExtra("id_pesanan") != null ? getIntent().getStringExtra("id_pesanan") : "";
        String strID        = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
        String strNama      = getIntent().getStringExtra("nama") != null ? getIntent().getStringExtra("nama") : "";
        String strTelepon   = getIntent().getStringExtra("telepon") != null ? getIntent().getStringExtra("telepon") : "";
        String strEmail     = getIntent().getStringExtra("email") != null ? getIntent().getStringExtra("email") : "";
        String strAlamat    = getIntent().getStringExtra("alamat") != null ? getIntent().getStringExtra("alamat") : "";
        String strTanggal   = getIntent().getStringExtra("tanggal") != null ? getIntent().getStringExtra("tanggal") : "";
        String strMerk      = getIntent().getStringExtra("merk") != null ? getIntent().getStringExtra("merk") : "";
        String strHarga     = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
        String strWarna     = getIntent().getStringExtra("warna") != null ? getIntent().getStringExtra("warna") : "";
        String strType      = getIntent().getStringExtra("type") != null ? getIntent().getStringExtra("type") : "";
        String strPembayaran= getIntent().getStringExtra("pembayaran") != null ? getIntent().getStringExtra("pembayaran") : "";
        String strImage     = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";

        //Format Rupiah
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
//        final int idDrawable = activity.getResources().getIdentifier(strImage, "drawable", activity.getPackageName());
//
//        imageView.setImageResource(idDrawable);

        // TODO: Cek Gambar
        if (strImage.equals("images_civic")){
            imageView.setImageResource(R.drawable.images_civic);
        }else if (strImage.equals("images_jazz_silver")){
            imageView.setImageResource(R.drawable.images_jazz_silver);
        }else if (strImage.equals("images_terios")){
            imageView.setImageResource(R.drawable.images_terios);
        }else if (strImage.equals("images_mclaren")){
            imageView.setImageResource(R.drawable.images_mclaren);
        }else if (strImage.equals("images_ford")){
            imageView.setImageResource(R.drawable.images_ford);
        }else if (strImage.equals("images_jazz")){
            imageView.setImageResource(R.drawable.images_jazz);
        }else if (strImage.equals("images_avanza")){
            imageView.setImageResource(R.drawable.images_avanza);
        }else if (strImage.equals("images_hrv")){
            imageView.setImageResource(R.drawable.images_hrv);
        }else if (strImage.equals("images_hrv_red")){
            imageView.setImageResource(R.drawable.images_hrv_red);
        }

        tvMerk.setText(strMerk);

        etIDPesanan.setText(strIdPesanan);
        etID.setText(strID);
        etName.setText(strNama);
        etTelepon.setText(strTelepon);
        etEmail.setText(strEmail);
        etAlamat.setText(strAlamat);
        etTanggal.setText(strTanggal);
        etMerk.setText(strMerk);
        etHarga.setText(formatRupiah.format((double) Integer.parseInt(strHarga))+",-");
        etWarna.setText(strWarna);
        etType.setText(strType);
        etPembayaran.setText(strPembayaran);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickSubmit();
            }
        });
    }

    private void ClickSubmit() {
        String strID        = etID.getText().toString().trim();
        String strNama      = etName.getText().toString().trim();
        String strTelepon   = etTelepon.getText().toString();
        String strEmail     = etEmail.getText().toString();
        String strAlamat    = etAlamat.getText().toString();
        String strTanggal   = etTanggal.getText().toString();
        String strMerk      = etMerk.getText().toString();
        String strWarna     = etWarna.getText().toString();
        String strType      = etType.getText().toString();
        String strPembayaran= etPembayaran.getText().toString();

        if (!Utilities.isValidForm(activity,strID,etID)){
            return;
        }
        if (!Utilities.isValidForm(activity,strNama,etName)){
            return;
        }
        if (!Utilities.isValidForm(activity,strTelepon,etTelepon)){
            return;
        }
        if (!Utilities.isValidForm(activity,strEmail,etEmail)){
            return;
        }
        if (!Utilities.isValidForm(activity,strAlamat,etAlamat)){
            return;
        }
        if (!Utilities.isValidForm(activity,strTanggal,etTanggal)){
            return;
        }
        if (!Utilities.isValidForm(activity,strMerk,etMerk)){
            return;
        }
        if (!Utilities.isValidForm(activity,strWarna,etWarna)){
            return;
        }
        if (strType.equals("- Pilih Type Mobil -")){
            Utilities.ShowToast(activity, "pilih type mobil");
            return;
        }
        if (strPembayaran.equals("- Pilih Metode Pembayaran -")){
            Utilities.ShowToast(activity, "pilih metode pembayaran");
            return;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Anda yakin data sudah benar?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        InsertToDatabaseTransaksi();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();

    }

    private void InsertToDatabaseTransaksi() {

        String strIdPesanan = etIDPesanan.getText().toString().trim();
        String strID        = etID.getText().toString().trim();
        String strNama      = etName.getText().toString().trim();
        String strTelepon   = etTelepon.getText().toString();
        String strEmail     = etEmail.getText().toString();
        String strAlamat    = etAlamat.getText().toString();
        String strTanggal   = etTanggal.getText().toString();
        String strMerk      = etMerk.getText().toString();
        String strWarna     = etWarna.getText().toString();
        String strType      = etType.getText().toString();
        String strPembayaran= etPembayaran.getText().toString();
        String strImage     = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";
        String strHarga     = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
        String strFormType  = getIntent().getStringExtra("formType") != null ? getIntent().getStringExtra("formType") : "";

        try {
            JSONObject obj = new JSONObject()
                    .put("id_pesanan", strIdPesanan)
                    .put("id", strID)
                    .put("nama", strNama)
                    .put("telepon", strTelepon)
                    .put("email", strEmail)
                    .put("alamat", strAlamat)
                    .put("tanggal", strTanggal)
                    .put("merk", strMerk)
                    .put("harga", strHarga)
                    .put("warna", strWarna)
                    .put("type", strType)
                    .put("pembayaran", strPembayaran)
                    .put("image", strImage);
            String dataJson = obj.toString();

            // TODO: INSERT DATA TABEL TRANSAKSI SQLITE
            SQLiteDatabase database = openOrCreateDatabase(DatabaseLabel.DB_NAME, Context.MODE_PRIVATE, null);
            database.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseLabel.TABLE_TRANSAKSI + "(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_PESANAN TEXT, DATA_JSON TEXT);");

            // TODO: Identifikasi input data baru/edit data yang sudah ada
            if (strFormType.equals("new_form")) {
                database.execSQL("INSERT OR REPLACE INTO " + DatabaseLabel.TABLE_TRANSAKSI + " (ID_PESANAN, DATA_JSON) VALUES('" + strIdPesanan + "','" + dataJson + "');");
            }else {
                database.execSQL("UPDATE " + DatabaseLabel.TABLE_TRANSAKSI + " SET DATA_JSON = '" + dataJson + "' WHERE ID_PESANAN = '" + strIdPesanan + "';");
            }
            database.close();
            AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
            alertDialog.setTitle("Pemesanan berhasil");
            alertDialog.setMessage("Cek riwayat pemesanan di menu Transaksi");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(activity, MainActivity.class));
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
