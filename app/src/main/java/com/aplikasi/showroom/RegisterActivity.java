package com.aplikasi.showroom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aplikasi.showroom.Utils.DatabaseLabel;
import com.aplikasi.showroom.Utils.SharedPrefManager;
import com.aplikasi.showroom.Utils.Utilities;
import com.aplikasi.showroom.model.User;

public class RegisterActivity extends AppCompatActivity {
    Activity activity;
    EditText etUsername, etPhone, etPassword, etRepeatPassword;
    TextView tvBackLogin;
    Button btnRegister;
    String TAG = "LOGGING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        activity = this;

        btnRegister = findViewById(R.id.btnRegister);
        tvBackLogin = findViewById(R.id.tv_back_login);
        etUsername  = findViewById(R.id.et_username);
        etPhone     = findViewById(R.id.et_phone);
        etPassword  = findViewById(R.id.et_password);
        etRepeatPassword = findViewById(R.id.et_repeatpassword);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickRegister();
            }
        });

        tvBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));

            }
        });

    }

    private void ClickRegister() {
        String username = etUsername.getText().toString();
        String phone    = etPhone.getText().toString();
        String password = etPassword.getText().toString();
        String repeat_password = etRepeatPassword.getText().toString();

        if (!Utilities.IsValidEmail(username)) {
            etUsername.setError("email tidak valid");
        } else if (!Utilities.validatePhone(phone)) {
            etPassword.setError("telepon tidak valid");

        } else if (!Utilities.validatePassword(password)) {
            etPassword.setError("password terlalu pendek");

        } else if (!Utilities.validateRepeatPassword(password, repeat_password)) {
            etRepeatPassword.setError("password tidak sama");

        } else {
            CreateAdmin(username, phone, password);
        }

    }

    public void CreateAdmin(String username, String phone, String password) {
        if (!isExisting(username, password)) {
            String idAdmin = Utilities.currentDateFormat();
            SQLiteDatabase database = openOrCreateDatabase(DatabaseLabel.DB_NAME, Context.MODE_PRIVATE, null);
            // TODO: CEK USER ADMIN FROM SQLITE
            database.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseLabel.TABLE_ADMIN + "(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_ADMIN TEXT, USERNAME TEXT, PASSWORD TEXT, PHONE TEXT);");
            database.execSQL("DELETE FROM " + DatabaseLabel.TABLE_ADMIN + " where USERNAME = '" + username + "' and PASSWORD = '" + password + "';");
            database.execSQL("INSERT OR REPLACE INTO " + DatabaseLabel.TABLE_ADMIN + " (ID_ADMIN, USERNAME, PASSWORD, PHONE) VALUES('" + idAdmin + "','" + username + "','" + password + "','" + phone + "');");
            database.close();

            //User usr = new User(idAdmin, username, phone);
            //SharedPrefManager.getInstance(getApplicationContext()).userLogin(usr);
            //startActivity(new Intent(getApplicationContext(), LoginActivity.class));

            String intent   = getIntent().getStringExtra("intent") != null ? getIntent().getStringExtra("intent") : "";
            String id       = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
            String image    = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";
            String mobil    = getIntent().getStringExtra("mobil") != null ? getIntent().getStringExtra("mobil") : "";
            String harga    = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
            String description = getIntent().getStringExtra("description") != null ? getIntent().getStringExtra("description") : "";

            Intent i = new Intent(activity, LoginActivity.class);
            i.putExtra("intent", intent);
            i.putExtra("id", id);
            i.putExtra("mobil", mobil);
            i.putExtra("image", image);
            i.putExtra("harga", harga);
            i.putExtra("description", description);
            startActivity(i);

            Utilities.ShowToast(activity, "Registrasi admin success");

        } else {
            Utilities.ShowToast(activity, "Data sudah ada");
        }
    }

    public Boolean isExisting(String username, String password) {
        SQLiteDatabase database = openOrCreateDatabase(DatabaseLabel.DB_NAME, Context.MODE_PRIVATE, null);
        // TODO: CEK USER ADMIN FROM SQLITE
        database.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseLabel.TABLE_ADMIN + "(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_ADMIN TEXT, USERNAME TEXT, PASSWORD TEXT, PHONE TEXT);");
        Cursor cursor = database.rawQuery("select * from " + DatabaseLabel.TABLE_ADMIN + " where USERNAME = '" + username + "' and PASSWORD = '" + password + "' limit 1;", null);

        if (cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                return true;
            }
        }
        cursor.close();
        database.close();
        return false;
    }

}
