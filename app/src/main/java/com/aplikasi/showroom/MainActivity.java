package com.aplikasi.showroom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.aplikasi.showroom.Utils.SharedPrefManager;
import com.aplikasi.showroom.Utils.Utilities;
import com.aplikasi.showroom.menu.Home.Home;
import com.aplikasi.showroom.menu.Info.Info;
import com.aplikasi.showroom.menu.Mobil.Mobil;
import com.aplikasi.showroom.menu.Transaksi.Transaksi;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity" ;
    Activity activity;
    private ProgressDialog pDialog;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout       = findViewById(R.id.sliding_tabs);
        viewPager       = findViewById(R.id.viewpager);
        pDialog         = new ProgressDialog(activity);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        pDialog.setCancelable(false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_logout);

        if (SharedPrefManager.getInstance(activity).isLoggedIn()){
            menuItem.setTitle("logout");
        } else {
            menuItem.setTitle("login");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_logout:
                SharedPrefManager.getInstance(getApplicationContext()).logout();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                //finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager cfManager = getSupportFragmentManager();
        adapter = new ViewPagerAdapter(cfManager);
        adapter.addFragment(new Home(), "Beranda");
        adapter.addFragment(new Mobil(), "Mobil");
        adapter.addFragment(new Transaksi(), "Transaksi");
        adapter.addFragment(new Info(), "info");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void updateTitle(int index, String title){
            mFragmentTitleList.set(index, title);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Log.e("report", mFragmentTitleList.get(position));
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ActivityCompat.finishAffinity(activity);
            } else {
                activity.finish();
            }
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Utilities.ShowToast(this, getResources().getString(R.string.double_click_exit));
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
