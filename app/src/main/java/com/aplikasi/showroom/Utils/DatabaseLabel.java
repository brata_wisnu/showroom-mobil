package com.aplikasi.showroom.Utils;

public interface DatabaseLabel {
    public String DB_NAME = "showroom";
    public String TABLE_ADMIN = "tbadmin";
    public String TABLE_TRANSAKSI = "tbtransaksi";
    public String TABLE_CUSTOMER = "tbcustomer";
    public String TABLE_MOBIL = "mobil";
}
