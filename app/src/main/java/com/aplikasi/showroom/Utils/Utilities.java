package com.aplikasi.showroom.Utils;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ucup on 25/08/17.
 */

public class Utilities {

    public static boolean IsValidEmail(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static Boolean isValidForm(Activity activity, String jawaban, EditText et) {
        if (TextUtils.isEmpty(jawaban)) {
            et.setError("form tidak boleh kosong");
            if (et.requestFocus()) {
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
            return false;
        } else
            return true;
    }

    public static boolean validatePassword(String password) {
        return password.length() > 5;
    }

    public static boolean validateRepeatPassword(String password, String repeat) {
        if (repeat.equals(password)){
            return true;
        }
        return false;
    }

    public static boolean validatePhone(String password) {
        return password.length() > 9;
    }

    public static void putPref(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void putPref(String key, int value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static String getPref(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static void putPrefName(String PREFS_NAME, String key, String value, Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putString(key, value); //3
        editor.commit(); //4
    }

    public static String getValuePrefName(String PREFS_NAME, String key, Context context) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        text = settings.getString(key, null); //2
        return text;
    }

    public static void clearSharedPreference(String PREFS_NAME, Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.commit();
    }


    public static void ShowLog(String message, String value) {
        Log.e("Restaurant", message + " : " + value);
    }

    public static void ShowToast(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

    }

    public static void ShowToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public static String currentDateFormat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    public static String ConvertDateFormat(String strDate) {
        String dateString="";
        if (!strDate.equals("")) {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
            SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy KK:mm a");
            Date date = null;
            try {
                date = format1.parse(strDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            dateString = format2.format(date);
        }
        return ((dateString));
    }

}
