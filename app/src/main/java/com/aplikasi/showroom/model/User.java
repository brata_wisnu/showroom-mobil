package com.aplikasi.showroom.model;

public class User {
    private String idAdmin;
    private String username;
    private String phone;


    public User() {

    }

    public User(String idAdmin, String username, String phone) {
        this.idAdmin = idAdmin;
        this.username = username;
        this.phone = phone;
    }


    public String getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(String idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
