package com.aplikasi.showroom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aplikasi.showroom.Utils.DatabaseLabel;
import com.aplikasi.showroom.Utils.SharedPrefManager;
import com.aplikasi.showroom.Utils.Utilities;
import com.aplikasi.showroom.menu.DetailMobil;
import com.aplikasi.showroom.menu.FormCustomerActivity;
import com.aplikasi.showroom.model.User;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    EditText etUsername, etPassword;
    TextView tvRegister;
    Activity activity;
    boolean doubleBackToExitPressedOnce = false;
    String TAG = "LOGGING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        activity = this;
        btnLogin    = findViewById(R.id.btnLogin);
        tvRegister  = findViewById(R.id.tvRegister);
        etUsername  = findViewById(R.id.et_username);
        etPassword  = findViewById(R.id.et_password);

        final String strIntent = getIntent().getStringExtra("intent") != null ? getIntent().getStringExtra("intent") : "";

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    ClickLogin(strIntent);
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strIntent = getIntent().getStringExtra("intent") != null ? getIntent().getStringExtra("intent") : "";
                String id       = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
                String image    = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";
                String mobil    = getIntent().getStringExtra("mobil") != null ? getIntent().getStringExtra("mobil") : "";
                String harga    = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
                String description = getIntent().getStringExtra("description") != null ? getIntent().getStringExtra("description") : "";

                Intent i = new Intent(activity, RegisterActivity.class);
                i.putExtra("intent", strIntent);
                i.putExtra("id", id);
                i.putExtra("mobil", mobil);
                i.putExtra("image", image);
                i.putExtra("harga", harga);
                i.putExtra("description", description);
                startActivity(i);

            }
        });
    }

    private void ClickLogin(String strIntent) {
        String username = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (!Utilities.IsValidEmail(username)) {
            etUsername.setError("email tidak valid");
        } else if (!Utilities.validatePassword(password)) {
            etPassword.setError("password tidak valid");

        } else {

            SQLiteDatabase database = openOrCreateDatabase(DatabaseLabel.DB_NAME, Context.MODE_PRIVATE, null);
            // TODO: CEK USER ADMIN FROM SQLITE
            database.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseLabel.TABLE_ADMIN + "(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_ADMIN TEXT, USERNAME TEXT, PASSWORD TEXT, PHONE TEXT);");
            Cursor cursor = database.rawQuery("select * from " + DatabaseLabel.TABLE_ADMIN + " where USERNAME = '" + username + "' and PASSWORD = '" + password + "' limit 1;", null);

            if (cursor.moveToFirst()) {
                if (cursor.getCount() > 0) {
                    String strIdAdmin   = cursor.getString(cursor.getColumnIndex("ID_ADMIN"));
                    String strUsername  = cursor.getString(cursor.getColumnIndex("USERNAME"));
                    String strPhone     = cursor.getString(cursor.getColumnIndex("PHONE"));

                    User usr = new User(strIdAdmin, strUsername, strPhone);
                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(usr);
                    //startActivity(new Intent(getApplicationContext(), MainActivity.class));

                    if (strIntent.equals("transaksi")){
                        finish();
                    }else {
                        String id       = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
                        String image    = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";
                        String mobil    = getIntent().getStringExtra("mobil") != null ? getIntent().getStringExtra("mobil") : "";
                        String harga    = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
                        String description = getIntent().getStringExtra("description") != null ? getIntent().getStringExtra("description") : "";

                        Intent i = new Intent(activity, DetailMobil.class);
                        i.putExtra("id", id);
                        i.putExtra("mobil", mobil);
                        i.putExtra("image", image);
                        i.putExtra("harga", harga);
                        i.putExtra("description", description);
                        startActivity(i);
                    }

                } else {
                    Toast.makeText(activity, "Username dan Password salah", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(activity, "Username dan Password salah", Toast.LENGTH_SHORT).show();
            }
            cursor.close();
            database.close();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null)
        if (SharedPrefManager.getInstance(getApplicationContext()).isLoggedIn()) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String intent   = getIntent().getStringExtra("intent") != null ? getIntent().getStringExtra("intent") : "";
        if (intent.equals("detailMobil")){
            String id       = getIntent().getStringExtra("id") != null ? getIntent().getStringExtra("id") : "";
            String image    = getIntent().getStringExtra("image") != null ? getIntent().getStringExtra("image") : "";
            String mobil    = getIntent().getStringExtra("mobil") != null ? getIntent().getStringExtra("mobil") : "";
            String harga    = getIntent().getStringExtra("harga") != null ? getIntent().getStringExtra("harga") : "0";
            String description = getIntent().getStringExtra("description") != null ? getIntent().getStringExtra("description") : "";

            Intent i = new Intent(activity, DetailMobil.class);
            i.putExtra("id", id);
            i.putExtra("mobil", mobil);
            i.putExtra("image", image);
            i.putExtra("harga", harga);
            i.putExtra("description", description);
            startActivity(i);
        }else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));

        }
//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                ActivityCompat.finishAffinity(activity);
//            } else {
//                activity.finish();
//            }
//            return;
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Utilities.ShowToast(this, getResources().getString(R.string.double_click_exit));
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 2000);
    }
}
